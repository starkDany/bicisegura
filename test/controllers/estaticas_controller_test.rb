require 'test_helper'

class EstaticasControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get politicas" do
    get :politicas
    assert_response :success
  end

  test "should get acercade" do
    get :acercade
    assert_response :success
  end

  test "should get faq" do
    get :faq
    assert_response :success
  end

  test "should get unete" do
    get :unete
    assert_response :success
  end

end
