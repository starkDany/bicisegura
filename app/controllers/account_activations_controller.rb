class AccountActivationsController < ApplicationController

  def edit
    usuario = Usuario.find_by(email: params[:email])
    if usuario && !usuario.activated? && usuario.authenticated?(:activation, params[:id])
      usuario.activate
      log_in usuario
      flash[:success] = "Cuenta activada!"
      redirect_to root_path
    else
      flash[:danger] = "Link de activación incorrecto"
      redirect_to root_url
    end
  end
end
