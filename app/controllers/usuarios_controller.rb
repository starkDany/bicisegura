class UsuariosController < ApplicationController
    before_action :logged_in_usuario, only: [:edit, :update, :show, :destroy]
    before_action :correct_usuario,   only: [:edit, :update, :show]
    before_action :admin_usuario,     only: [:index, :destroy]

  def index
    @usuarios = Usuario.paginate(page: params[:page], :per_page => 20)
  end

  def show
	@usuario = Usuario.find(params[:id])
  @microposts = @usuario.microposts.paginate(page: params[:page])
  end

  def nueva
  	@usuario = Usuario.new
  end

  def create
  	@usuario = Usuario.new(usuario_params)
  	if @usuario.save
      @usuario.send_activation_email
      flash[:info] = "Por favor checa tu email para activar tu cuenta"
      redirect_to root_url
  	else
  		render 'nueva'
  	end
  end

  def edit
    @usuario = Usuario.find(params[:id])
  end

  def update
    @usuario = Usuario.find(params[:id])
    if @usuario.update_attributes(usuario_params)
      flash[:success] = "Perfil actualizado"
      redirect_to @usuario
      # Handle a successful update.
    else
      render 'edit'
    end
  end

  def destroy
    Usuario.find(params[:id]).destroy
    flash[:success] = "Usuario borrado"
    redirect_to usuarios_url
  end

  private

  	def usuario_params
  		params.require(:usuario).permit(:nombre, :email, :password, :password_confirmation)
  	end

    

    def correct_usuario
      @usuario = Usuario.find(params[:id])
      redirect_to(root_url) unless current_usuario?(@usuario)
    end

end
