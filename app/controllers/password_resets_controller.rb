class PasswordResetsController < ApplicationController
	before_action :get_usuario,   only: [:edit, :update]
	before_action :valid_usuario, only: [:edit, :update]
	before_action :check_expiration, only: [:edit, :update]
  def new
  end

  def create
    @usuario = Usuario.find_by(email: params[:password_reset][:email].downcase)
    if @usuario
      @usuario.create_reset_digest
      @usuario.send_password_reset_email
      flash[:info] = "Te mandamos un mail con instrucciones secretas para que obtengas un nuevo password."
      redirect_to root_url
    else
      flash.now[:danger] = "No existe ese mail en nuestra base!"
      render 'new'
    end
  end

  def edit
  end

  def update
    if params[:usuario][:password].empty?
      flash.now[:danger] = "El password no puede estar en blanco"
      render 'edit'
    elsif @usuario.update_attributes(usuario_params)
      log_in @usuario
      flash[:success] = "¡Felicidades! Tienes nuevo password :)"
      redirect_to @usuario
    else
      render 'edit'
    end
  end

  private

  def usuario_params
      params.require(:usuario).permit(:password, :password_confirmation)
    end

  def get_usuario
      @usuario = Usuario.find_by(email: params[:email])
  end

  # Confirms a valid user.
	def valid_usuario
	  unless (@usuario && @usuario.activated? &&
	          @usuario.authenticated?(:reset, params[:id]))
	    
	  end
	end

	# Checks expiration of reset token.
    def check_expiration
      if @usuario.password_reset_expired?
        flash[:danger] = "Ya expiró el link :("
        redirect_to new_password_reset_url
      end
    end

end
