module SessionsHelper

  # Logs in the given user.
  def log_in(usuario)
    session[:usuario_id] = usuario.id
  end

  # Remembers a user in a persistent session.
  def remember(usuario)
    usuario.remember
    cookies.permanent.signed[:usuario_id] = usuario.id
    cookies.permanent[:remember_token] = usuario.remember_token
  end

  # Returns true if the given user is the current user.
  def current_usuario?(usuario)
    usuario == current_usuario
  end

  def current_usuario
    if (usuario_id = session[:usuario_id])
      @current_usuario ||= Usuario.find_by(id: usuario_id)
    elsif (usuario_id = cookies.signed[:usuario_id])
      usuario = Usuario.find_by(id: usuario_id)
      if usuario && usuario.authenticated?(:remember, cookies[:remember_token])
        log_in usuario
        @current_usuario = usuario
      end
    end
  end

  def logged_in?
    !current_usuario.nil?
  end

  def forget(usuario)
    usuario.forget
    cookies.delete(:usuario_id)
    cookies.delete(:remember_token)
  end

  def log_out
  	forget(current_usuario)
    session.delete(:usuario_id)
    @current_usuario = nil
  end

  # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end

end






