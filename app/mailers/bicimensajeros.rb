class Bicimensajeros < ApplicationMailer

	def post_email( usuario, micropost )
	@usuario = usuario
	@micropost = micropost

    mail(:from => "#{usuario.nombre} <#{usuario.email}>", :to => "#{usuario.nombre} <#{usuario.email}>", :bcc => "somosmajaderos@gmail.com",  :subject => "Tu bicimensajero va en camino")

   end

end
