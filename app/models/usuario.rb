class Usuario < ActiveRecord::Base
  has_many :microposts, dependent: :destroy
	attr_accessor :remember_token, :activation_token, :reset_token
	before_save   :downcase_email
	before_create :create_activation_digest
	validates :nombre, presence: true, length: { maximum: 50 }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX },
								uniqueness: { case_sensitive: false }
	has_secure_password
	validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

	def Usuario.digest(string)
	    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
	                                                  BCrypt::Engine.cost
	    BCrypt::Password.create(string, cost: cost)
  	end
	
	def Usuario.new_token
		SecureRandom.urlsafe_base64
  	end

  	def remember
	    self.remember_token = Usuario.new_token
	    update_attribute(:remember_digest, Usuario.digest(remember_token))
  	end

  	def authenticated?(attribute, token)
  		digest = send("#{attribute}_digest")
  		return false if digest.nil?
	    BCrypt::Password.new(digest).is_password?(token)
	end

	# Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Activates an account.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UsuarioMailer.account_activation(self).deliver_now
  end

  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = Usuario.new_token
    update_attribute(:reset_digest,  Usuario.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UsuarioMailer.password_reset(self).deliver_now
  end

  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  def feed
    Micropost.where("usuario_id = ?", id)
  end

  private

  # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = Usuario.new_token
      self.activation_digest = Usuario.digest(activation_token)
    end

end
