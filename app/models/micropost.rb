class Micropost < ActiveRecord::Base
  belongs_to :usuario
  after_create :send_email
  default_scope -> { order(created_at: :desc) }
  validates :usuario_id, presence: true
  validates :callere, presence: true
  validates :num_ext_re, presence: true
  validates :num_int_re, presence: true
  validates :colonia_re, presence: true
  validates :delegacion_re, presence: true
  validates :cp_re, presence: true
  validates :remitente, presence: true
  validates :calle_de, presence: true
  validates :num_ext_de, presence: true
  validates :num_int_de, presence: true
  validates :colonia_de, presence: true
  validates :delegacion_de, presence: true
  validates :cp_de, presence: true
  validates :destinatario, presence: true
  validates :precio, presence: true
  validates :notas, presence: true
  validates :telefono, presence: true
end

def send_email
   Bicimensajeros.post_email( self.usuario, self ).deliver
 end