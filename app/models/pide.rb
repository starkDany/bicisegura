class Pide < MailForm::Base
  attribute :remitente, :validate => true
  attribute :calle, :validate => true
  attribute :numeroext, :validate => true
  attribute :numeroint, :validate => true
  attribute :colonia, :validate => true
  attribute :delegacion, :validate => true
  attribute :codpost, :validate => true
  attribute :destinatario, :validate => true
  attribute :calledest, :validate => true
  attribute :numeroextdest, :validate => true
  attribute :numerointdest, :validate => true
  attribute :coloniadest, :validate => true
  attribute :delegaciondest, :validate => true
  attribute :codpostdest, :validate => true
  attribute :comentario, :validate => true
  attribute :precio, :validate => true
  attribute :distancia, :validate => true
  attribute :pago, :validate => true
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :telefono, :validate => true
  attribute :datos, :validate => true
  attribute :nickname,  :captcha  => true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => "Pedido confirmado - Bicimensajero",
      :to => %(<#{email}>),
      :cc => "daniel@bicimensajero.com",
      :bcc => "danielvp1987@gmail.com",
      :from => %(<#{email}>)
    }
  end
end