class CreateMicroposts < ActiveRecord::Migration
  def change
    create_table :microposts do |t|
      t.string :callere
      t.string :num_ext_re
      t.string :num_int_re
      t.string :colonia_re
      t.string :delegacion_re
      t.string :cp_re
      t.string :remitente
      t.string :calle_de
      t.string :num_ext_de
      t.string :num_int_de
      t.string :colonia_de
      t.string :delegacion_de
      t.string :cp_de
      t.string :destinatario
      t.string :precio
      t.text :notas
      t.string :telefono
      t.references :usuario, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :microposts, [:usuario_id, :created_at]
  end
end
