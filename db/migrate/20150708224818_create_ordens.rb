class CreateOrdens < ActiveRecord::Migration
  def change
    create_table :ordens do |t|
      t.text :pickup
      t.text :dropoff
      t.integer :distancia
      t.string :precio

      t.timestamps null: false
    end
  end
end
